<?php
/*
 * 头部文件
 */
header("content-type:text/html;charset=utf-8");
include_once '/comm/comm.inc.php';
//防止非法调用
if(AN_IL_CAL != 'an_illegal_call'){
    echo "非法操作！";
    exit;
}
?>
<!DOCTYPE  html>
<html>
    <head>
        <title>AskY</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
        <link href="css/header.css" rel="stylesheet" type="text/css">
        <link href="css/footer.css" rel="stylesheet" type="text/css">
        <link href="css/main.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="header">
            <div class="h_wrap">
                <div class="web_name">AskY</div>
                        <form class="h_form" id="h_form" name="h_se_form">
                            <input type="image" class="img_sub" src="images/search1.png">
                            <input type="text" class='s_text' id="s_text" name="h_search" placeholder="输入关键字搜索">
                        </form>
                        <div class="nav">
                            <ul>
                                <li>问题</li>
                                <li>文章</li>
                                <li>话题</li>
                            </ul>
                        </div>
                        <div class="nav_full" id="nav_full" style='display: none;'>
                            <ul>
                                <li>问题</li>
                                <li>文章</li>
                                <li>话题</li>
                            </ul>
                        </div>
                <?php if(isset($_SESSION['uname'])){?>
                        <div class="info">
                            <div class="h_list">撰写</div>
                            <div class="h_second_list" style="display: none">
                                <ul>
                                    <li>1</li>
                                    <li>1</li>
                                    <li>2</li>
                                </ul>
                            </div>
                            <div class="img_m"><img src="images/message.png"></div>
                            <div class="h_img"><a href="profit.php">ere</a></div>
                            <div class="h_img_list" style="display: none">
                                <ul>
                                    <li>1</li>
                                    <li>1</li>
                                    <li>2</li>
                                </ul>
                            </div>
                        </div>
                        <a class="meun_ic" id="meun_ic">Ξ</a>
            <?php }else{?>
                        <div class="info">
                            <div class="h_list"><a class="h_list" href="login.php">登录</a></div>
                            <div class="h_list"><a class="h_list" href="register.php">注册</a></div>
                        </div>
                        <a class="meun_ic" id="meun_ic">Ξ</a>
            <?php }?>
        </div>
</div>
    <script type="text/javascript">
            var s_text = document.getElementById('s_text');
            var f = document.getElementById('h_form');
            s_text.onfocus = function(){
                f.style.boxShadow = '4px 4px 4px #1e60ff';
            }
            s_text.onblur = function(){
                f.style.boxShadow = 'none';
            }
            
            var meun_ic = document.getElementById('meun_ic');
            var nav_full = document.getElementById('nav_full');
            meun_ic.onclick = function(){
            	if(nav_full.style.display == 'none'){
            		nav_full.style.display = 'block';
            	}else{
            		nav_full.style.display = 'none';
            	}
            }
            
        </script>