<?php
/*注册页面*/
//引入公共文件
require_once ("comm/comm.inc.php");

//登录检测

login_check();

if($_GET['act'] == 'check_reg'){
    //获取数据
    $data['uname'] = check_uname($r_uname);
    $data['password'] = md5(check_password($r_upwd));
    $data['email'] = check_email($r_email);

    $sql = "select user_name from users where user_name = '{$data['uname']}' ";
    $query = mysql_query($sql);
    if(mysql_fetch_array($query,MYSQL_ASSOC)){
        message_show("此用户名已存在！","register.php");
    }

    mysql_query("insert into users (user_name,password,email) values (
                                                                    '{$data['uname']}',
                                                                    '{$data['password']}',
                                                                    '{$data['email']}'
              )") or die('注册失败，请重试！');
    if(mysql_affected_rows() == 1){
        mysql_close();
        session_destroy();
        message_show('注册成功！','login.php');
    }else{
        mysql_close();
        session_destroy();
        message_show('注册失败！请重试！','register.php');
    }
}

?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>AskY</title>
    <link href="css/login_n_sign.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrap">
    <div class="l_head">
        <div class="logo"><span>AskY</span><br><span class="second_h">small AskY</span></div>
        <div class="login">
            <form name="login" action="register.php?act=check_reg" method="post">
                <span class="s_one">注册AskY</span>
                <img src="images/arrow1.png"><span class="s_two"><a href="login.php">登录</a></span>
                <br>
                <input class="tt r_uname" type="text" name="r_uname" placeholder="姓名">
                <br>
                <input class="tt r_email" type="text" name="r_email" placeholder="邮箱">
                <br>
                <input class="tt r_upwd" type="password" name="r_upwd" placeholder="密码（不少于6位）">
                <br>
                <input class="tt sub" type="submit" value="注册AskY">
            </form>
        </div>
    </div>
</div>
<div class="l_con"></div>
<div class="l_footer"></div>
</body>
</html>
